package com.zuitt.s01a1;

public class Activity {
    public static void main(String[] args) {
        String firstName = "Juan";
        String lastName = "Dela Cruz";
        double englishGrade = 85.6;
        double mathGrade = 88.6;
        double scienceGrade = 92.6;
        double gradeAverage = (englishGrade + mathGrade + scienceGrade)/3;

        System.out.println(firstName + " " + lastName + "'s " + "average is " + gradeAverage);
    }

}
